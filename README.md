# Ateliers pour les Méthodes d'Informations Archéologiques et Outils Utiles (MIAOU)
 
<center>

![patte de chat](images/patte_de_chat.png "une patte de chat")

</center>

Bienvenue sur la page des ateliers du MIAOU



Vous retrouverez ici les documents relatifs aux sessions ayant eu lieu.

Et c'est ici pour le [planning](https://evento.renater.fr) :cat:

Et la liste des session ici avec leur thèmes.

Prochaine date le : 2 octobre - Amphithéatre Malher